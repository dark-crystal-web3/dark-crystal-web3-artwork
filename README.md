
## dark-crystal-web3-artwork

Custom icons and background images for Dark Crystal Web3 user interface.

By: [Rubén Cruces Pérez](https://www.instagram.com/rubencrucesperez/) leupeso@gmail.com 

License: AGPLv3

![](./darkcrystalcave1700px.png)

![](./cavepixelart.jpg)

![](./cavepixelarttrnsp.jpg)

![](./cavepixelarttrnsp3.jpg)

![](./achievemnt.png)

![](./cofre-abierto.png)

![](./cofre-cerrado.png)

![](./copypapers.png)

![](./cristal-bold.png)

![](./cristal-light.png)

![](./cristal-lightb.png)

![](./cristal-lightbc.png)

![](./cristal-lightg.png)

![](./cristal-lightp.png)

![](./cristal-lightr.png)

![](./cristalsplenty.png)

![](./ethereum-logo.png)

![](./key.png)

![](./key.psd)

![](./key44px.png)

![](./masterkey44px.png)
